
# hannaford

Query and Post Data to the ‘Hannaford’ ‘API’

## Description

[Hannaford](https://en.wikipedia.org/wiki/Hannaford_Brothers_Company) is
a supermarket chain with locations primarily in New England.

They have a “[To Go](https://www.hannaford.com/togo)” service which lets
you shop online and pick up orders at the store. You can also make a
grocery list online and use that when shopping. You’re supposed to be
able to scan your “number” and have groceries tracked automagically for
you after check out wihtout said list but I haven’t gotten that to work
at my very rural store. Finally, they have a process by where you can
enter receipt information into a form and retrieve items purchased.

Said rural store is about a 9 minute drive for me, and it changed where
I shopped for most everyday items that aren’t in specialty shops or
local farmer’s markets.

Since I buy the vast majority of food-items at Hannaford, now, I thought
it might be fun to track the purchases in 2018 and see if such a
data-driven view might help us make better or different choices. I also
want to see how much better our bill gets once \#3 goes to college.

So, we’re literally putting the “R” in Hannaford.

This is a *fragile* package since it currently behaves like a browser
and does a great deal of scraping. I also need to map out more of the
“API” to enable scraping of categories and associating items to said
categories.

I also plan to make a dead-simple “sync” function which will take care
of all the details to help maintain a local (SQLite) database. This is
important since Hannaford removes receipt detail after 30 days.

For the most streamlined operation, you must/should have both of the
following environment variables available:

  - `HANNAFORD_USERNAME`
  - `HANNAFORD_PASSWORD`

The easiest way to do this is to store the values in your `~/.Renviron`
file.

## What’s Inside The Tin

The following functions are implemented:

  - `enter_receipt`: Enter receipt metadata to trigger back-end
    processing at Hannaford
  - `hannaford_login`: Login to the Hannaford API
  - `order_detail`: Retrieve order detail
  - `purchase_history`: Retrieve purchase history (top-level metadata)
  - `receipt_status`: Retrieve receit processing status

## Installation

``` r
devtools::install_github("hrbrmstr/hannaford")
```

## Usage

COMING SOON (but the functions kinda are self-explanatory)

``` r
library(hannaford)

# current verison
packageVersion("hannaford")
```

    ## [1] '0.1.0'
