#' Login to the Hannaford API
#'
#' Create a Hannaford API login session.
#'
#' For the most streamlined operation, you must/should have both of the following
#' environment variables available:
#'
#' - `HANNAFORD_USERNAME`
#' - `HANNAFORD_PASSWORD`
#'
#' The easiest way to do this is to store the values in your `~/.Renviron` file.
#'
#' @md
#' @param username,password `hannaford.com` credentials
#' @note While this can be called directly, it will be used by all the other
#'       API interaction functions.
#' @return logical for success (invisibly)
#' @export
#' @examples \dontrun{
#' hannaford_login()
#' }
hannaford_login <- function(username=Sys.getenv("HANNAFORD_USERNAME"),
                            password=Sys.getenv("HANNAFORD_PASSWORD")) {

  # have to start at the beginning to get a CSRF token

  res <- httr::GET("https://www.hannaford.com/user/main.jsp")
  pg <- httr::content(res)

  csrf_token_header <- html_attr(html_node(pg, "input[name='CSRF_TOKEN_HEADER']"), "value")

  # now we need to use said token along with login info

  POST(
    url = "https://www.hannaford.com/user/login.cmd",
    body = list(
      form_state = "loginForm",
      CSRF_TOKEN_HEADER = csrf_token_header,
      isFromHeader = "true",
      dest = "https://www.hannaford.com/user/main.jsp",
      loginAction = "TRUE",
      userName = username,
      password = password,
      keepLoggedIn = "true"
    ),
    encode = "form"
  ) -> res

  httr::stop_for_status(res) # 200 shld be sufficient for login success but we'll use another one

  res <- httr::content(res)

  success_test <- html_text(html_node(res, "a.sign-out-link"))

  if (is.na(success_test)) return(invisible(FALSE))

  return(invisible(success_test == "Sign Out"))

}
