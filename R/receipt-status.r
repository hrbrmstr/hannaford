#' Retrieve receit processing status
#'
#' Query the Hannaford API and retrieve a data frame that includes receipt status information.
#'
#' For the most streamlined operation, you must/should have both of the following
#' environment variables available:
#'
#' - `HANNAFORD_USERNAME`
#' - `HANNAFORD_PASSWORD`
#'
#' The easiest way to do this is to store the values in your `~/.Renviron` file.
#'
#' @md
#' @param username,password `hannaford.com` credentials
#' @export
receipt_status <- function(username=Sys.getenv("HANNAFORD_USERNAME"),
                           password=Sys.getenv("HANNAFORD_PASSWORD")) {

  success <- hannaford_login(username, password)
  if (!success) stop("Error logging into hannaford.com", call.=FALSE)

  GET(
    url = "https://www.hannaford.com/user/view_store_receipt_status.jsp",
    httr::add_headers(
      `Referer` = "https://www.hannaford.com/user/enter_store_receipt_confirmation.jsp"
    )
  ) -> res

  httr::stop_for_status(res)

  pg <- httr::content(res)

  tickets <- html_node(pg, xpath=".//table[contains(., 'Ticket Number')]")

  data_frame(
    receipt = html_text(html_nodes(tickets, xpath=".//td[1]"), trim=TRUE), # receipt
    date = lubridate::mdy(html_text(html_nodes(tickets, xpath=".//td[2]"), trim=TRUE)), # date
    status = html_text(html_nodes(tickets, xpath=".//td[3]"), trim=TRUE), # status
    order_link = map_chr(html_nodes(tickets, xpath=".//td[4]"), ~html_attr(html_node(.x, xpath=".//a"), "href")) # link to order
  )

}
