#' Retrieve purchase history (top-level metadata)
#'
#' For the most streamlined operation, you must/should have both of the following
#' environment variables available:
#'
#' - `HANNAFORD_USERNAME`
#' - `HANNAFORD_PASSWORD`
#'
#' The easiest way to do this is to store the values in your `~/.Renviron` file.
#'
#' @md
#' @param username,password `hannaford.com` credentials
#' @export
purchase_history <- function(username=Sys.getenv("HANNAFORD_USERNAME"),
                             password=Sys.getenv("HANNAFORD_PASSWORD")) {

  success <- hannaford_login(username, password)
  if (!success) stop("Error logging into hannaford.com", call.=FALSE)

  GET(
    url = "https://www.hannaford.com/user/instore_order_history.jsp",
    httr::add_headers(
      `Referer` = "https://www.hannaford.com/user/main.jsp"
    )
  ) -> res

  httr::stop_for_status(res)

  pg <- httr::content(res)

  data_frame(
    store = gsub("[[:space:]]+", " ", html_text(
      html_nodes(html_node(pg, "div.storeOrdersWrap table"), xpath=".//td[1]"), trim=TRUE)), # store name
    date = gsub("[[:space:]]+", " ", html_text(
      html_nodes(html_node(pg, "div.storeOrdersWrap table"), xpath=".//td[2]"), trim=TRUE)), # purchase date
    amount = gsub("[[:space:]]+", " ", html_text(
      html_nodes(html_node(pg, "div.storeOrdersWrap table"), xpath=".//td[3]"), trim=TRUE)), # purchase amount
    stars = gsub("[[:space:]]+", " ", html_text(
      html_nodes(html_node(pg, "div.storeOrdersWrap table"), xpath=".//td[5]"), trim=TRUE)), # guiding stars
    detail_link = html_attr(html_nodes(
      html_node(pg, "div.storeOrdersWrap table"), xpath=".//td[6]/a"), "href") # order summary link
  ) -> out

  out$date <- lubridate::mdy(out$date)
  out$amount <- as.numeric(gsub("^\\$", "", out$amount))
  out$stars <- as.numeric(out$stars)

  out

}
