#' Retrieve order detail
#'
#' Returns a data frame of purchases
#'
#' For the most streamlined operation, you must/should have both of the following
#' environment variables available:
#'
#' - `HANNAFORD_USERNAME`
#' - `HANNAFORD_PASSWORD`
#'
#' The easiest way to do this is to store the values in your `~/.Renviron` file.
#'
#' @md
#' @param order_link link path fragment for an order retrieved through other methods
#' @param username,password `hannaford.com` credentials
#' @export
order_detail <- function(order_link,
                         username=Sys.getenv("HANNAFORD_USERNAME"),
                         password=Sys.getenv("HANNAFORD_PASSWORD")) {

  success <- hannaford_login(username, password)
  if (!success) stop("Error logging into hannaford.com", call.=FALSE)

  GET(
    url = "https://www.hannaford.com",
    path = order_link,
    httr::add_headers(
      `Referer` = "https://www.hannaford.com/user/enter_store_receipt_confirmation.jsp"
    )
  ) -> res

  httr::stop_for_status(res)

  pg <- httr::content(res)

  list(

    total = html_text(html_node(pg, xpath=".//p[contains(., 'Purchase Total')]")),

    store_info = html_text(html_nodes(pg, xpath=".//div[@class='billing-address-container' and
           contains(., 'Purchase Date')]/p[not(contains(., 'Purchase Date'))]")), # store info

    purcahse_date = lubridate::mdy(gsub(
      "^Purchase Date:\ *", "",
      html_text(html_node(pg, xpath=".//p[contains(., 'Purchase Date')]"))
    )),

    items = data_frame(
      # html_attr(html_nodes(pg, "input[name='productVariantCount']"), "value"),
      qty = as.numeric(html_attr(html_nodes(pg, "input[name='quantity']"), "value")),
      product_id = html_attr(html_nodes(pg, "input[name='productVariantId']"), "value"),
      product_name = html_attr(html_nodes(pg, "input[name='productName']"), "value"),
      # html_attr(html_nodes(pg, "input[name='productNameOriginal']"), "value"),
      uom = html_attr(html_nodes(pg, "input[name='unitOfMeasure']"), "value"),
      price = as.numeric(html_attr(html_nodes(pg, "input[name='displayPrice']"), "value")),
      msrp = as.numeric(html_attr(html_nodes(pg, "input[name='displayMsrp']"), "value")),
      # stars = map_chr(html_nodes(pg, "div.iconsWrapper"), ~html_attr(html_node(.x, "img"), "alt")), # guiding stars
      category = html_attr(html_nodes(pg, "input[name='categoryId']"), "value"),
      parent_category = html_attr(html_nodes(pg, "input[name='parentCategoryId']"), "value"),
      product_link=html_attr(html_nodes(pg, "a.productName"), "href")
    )

  )

}