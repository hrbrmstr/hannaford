#' Enter receipt metadata to trigger back-end processing at Hannaford
#'
#' For the most streamlined operation, you must/should have both of the following
#' environment variables available:
#'
#' - `HANNAFORD_USERNAME`
#' - `HANNAFORD_PASSWORD`
#'
#' The easiest way to do this is to store the values in your `~/.Renviron` file.
#'
#' @md
#' @param store_number,trans_date,register,ticket All the data you'd enter on the web
#' @param notify whether you want to be notified by e-mail when the receipt is processed
#' @param username,password `hannaford.com` credentials
#' @return logical (invisibly) upon success or failure. Messages will also be provided.
#' @export
enter_receipt <- function(store_number, trans_date, register, ticket, notify=TRUE,
                          username=Sys.getenv("HANNAFORD_USERNAME"),
                          password=Sys.getenv("HANNAFORD_PASSWORD")) {

  success <- hannaford_login(username, password)
  if (!success) stop("Error logging into hannaford.com", call.=FALSE)

  trans_date <- as.Date(trans_date)

  POST(
    url = "https://www.hannaford.com/user/enter_store_receipt.cmd",
    httr::add_headers(
      `Referer` = "https://www.hannaford.com/user/enter_store_receipt.jsp"
    ),
    body = list(
      form_state = "enterReceipt",
      storeNumber=store_number,
      transactionMonth=lubridate::month(trans_date),
      transactionDay=lubridate::day(trans_date),
      transactionYear=lubridate::year(trans_date),
      registerNumber=register,
      ticketNumber=ticket,
      emailNotification=notify,
      submit.x="18",
      submit.y="5"
    ),
    encode = "form"
  ) -> res

  httr::stop_for_status(res)

  res <- httr::content(res)

  msg <- html_text(html_node(res, "p.msgGeneral"))
  errs <- grep("high", html_text(html_nodes(res, "p.common-error")), value=TRUE, invert=TRUE)

  success <- !is.na(msg)
  if (!success) { msg <- paste0(errs, collapse="\n") }

  message(msg)

  return(invisible(success))

}
